<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\WindTurbine;

class WindTurbineController extends Controller
{
    public function index()
    {
        return WindTurbine::all();
    }
}
