import React, {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

function WindTurbine() {
    const [items, setItems] = useState([]);
    const getInspectionData = () => {
        axios.get('api/windturbine')
        .then( res => {
            if(res.status === 200){
                setItems(res.data);
            }
        })
        .catch( err => console.log(err));
    }

    const run = async () => {
        for(let i = 0; i < items.length; i++){
            await sleep(1200);
            processItem(items[i]);
            if(i === (items.length - 1)){
                cleanWindTurbine();
            }
        }
    }

    const processItem = async (item) => {
        if(item.id % 3 === 0 && item.id % 5 === 0){
            output("Coating Damage and Lightning Strike", 'cdls');
        }else if(item.id % 3 === 0){
            output("Coating Damage", 'cd');        
        }else if(item.id % 5 === 0){
            output("Lightning Strike", 'ls');
        }else{
            output(item.id);
        }
    }


    const cleanWindTurbine = () => {
        document.querySelector('.rotor').style.animationName = 'none';
    }

    const sleep = (milliseconds) => {
      return new Promise(resolve => setTimeout(resolve, milliseconds))
    }
    const output = (val, cls) => {
        document.getElementById('output').innerHTML = `<div class="col-sm-6 col-xs-12 col-md-4"><div class="card ${cls}">${val}</div></div>`;
        updateWindTurbineColor(cls);
    }

    const updateWindTurbineColor = (cls) => {
        let color = '';
        switch(cls){
            case 'cdls':
                color = '#D2691E';
                break;
            case 'cd':
                color = 'gold';
                break;
            case 'ls':
                color = '#DC143C';
                break;
        }
        let headChildNodes = document.head.childNodes;
        let lastHeadChildNode = headChildNodes[headChildNodes.length - 1]; 
        if(lastHeadChildNode.tagName === 'STYLE'){
            document.head.removeChild(lastHeadChildNode);
        } 
        let styleElem = document.head.appendChild(document.createElement("style"));
        styleElem.innerHTML += `.blade:after {border-top: 2em solid ${color} !important;}`;
        styleElem.innerHTML += `.blade:before {background: ${color} !important;}`;
        styleElem.innerHTML += `.blade {background: ${color} !important;}`;
        styleElem.innerHTML += `.rotor {background: ${color} !important;}`;
    }

    useEffect( () => {
        getInspectionData();
    }, [])

    useEffect( () => {
        run();
    }, [items])

    return (
        <div className="container">
            <div className="windTurbine">
                <div className="rotor">
                    <div className="blade b1"></div>
                    <div className="blade b2"></div>
                    <div className="blade b3"></div>
                </div>
                <div className="leg"></div>
            </div>
            <div id="output"></div>
        </div>
    );
}

export default WindTurbine;

if (document.getElementById('app')) {
    ReactDOM.render(<WindTurbine />, document.getElementById('app'));
}
